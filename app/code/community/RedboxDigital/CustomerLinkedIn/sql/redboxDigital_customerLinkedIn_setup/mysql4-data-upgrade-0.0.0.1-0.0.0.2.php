<?php
/** @var RedboxDigital_CustomerLinkedIn_Model_Resource_Setup  $installer */

$installer = $this;
$eavConfig = Mage::getSingleton('eav/config');
$attributeCode = 'linkedin_profile';
$entity = $installer->getEntityTypeId('customer');
$store = Mage::app()->getStore(Mage_Core_Model_App::ADMIN_STORE_ID);
if($installer->isAttributeExists($entity, $attributeCode)) {

    $validationExtraRule = array(
        'sort_order'        => 190,
        'is_user_defined'   => 0,
        'is_system'         => 1,
        'is_visible'        => 1,
        'is_required'       => Mage::getStoreConfig('customer/linkedin/is_required', $store) == '' ? 0 : 1,
        'class' => 'validate-url validate-length maximum-length-250',
        'validate_rules'    => array(
            'max_text_length'   => 250,
            'min_text_length'   => 0,
            'input_validation' => 'url'
        ));

    $attribute = $eavConfig->getAttribute('customer', $attributeCode);
    $attribute->addData($validationExtraRule);;
    $usedInForms = array(
        'adminhtml_customer',
        'customer_account_create',
        'customer_account_edit',
        'checkout_register'
    );

    $attribute->setData('used_in_forms', $usedInForms)->save();
}