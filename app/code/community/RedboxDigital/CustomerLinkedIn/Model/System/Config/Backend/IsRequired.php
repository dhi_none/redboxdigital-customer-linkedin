<?php

/**
 * Backend model attach to config  customer/linked
 * Class RedboxDigital_CustomerLinkedIn_Model_System_Config_Backend_IsRequired
 * @package RedboxDigital
 * @author     Dhiraj Gangoosirdar <dhirajmetal@gmail.com>
 */

class RedboxDigital_CustomerLinkedIn_Model_System_Config_Backend_IsRequired  extends Mage_Core_Model_Config_Data
{
    /**
     * trigger after config customer/linkedin/is_required value is save
     * @return Mage_Core_Model_Abstract|void
     */

    public function _afterSave()
    {
        if ($this->isValueChanged()) {
            $value = $this->getValue();
            $attributeCode = 'linkedin_profile';

            /** @var RedboxDigital_CustomerLinkedIn_Model_Resource_Setup  $installer */
            $eavSetupCustomer = Mage::getModel('redboxDigital_customerLinkedIn/resource_setup', 'core_setup');
            $entity = $eavSetupCustomer->getEntityTypeId('customer');

            if($eavSetupCustomer->isAttributeExists($entity, $attributeCode)) {
                $eavSetupCustomer->updateAttribute('customer',$attributeCode,'is_required',$value);

            }

        }
    }
}