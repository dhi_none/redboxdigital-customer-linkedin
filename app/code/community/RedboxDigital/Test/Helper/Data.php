<?php
class RedboxDigital_CustomerLinkedIn_Test_Helper_Data extends  EcomDev_PHPUnit_Test_Case
{

    /**
     * Test that product removals is allowed from checkout
     *
     * @param string $store
     * @test
     * @covers RedboxDigital_CustomerLinkedIn_Test_Helper_Data::isAttributeRequired
     * @loadExpectation settings
     * @dataProvider dataProvider
     */

    protected $helper = null;

    protected function setUp()
    {
        parent::setUp();
        $this->helper = Mage::helper('redboxDigital_customerLinkedIn');
    }

    public function isAttributeRequired($store)
    {
        $this->setCurrentStore($store);
        $this->assertEquals(
            $this->expected($store)->getIsAttributeRequired(),
            $this->helper->isAttributeRequired($this->expected($store)->getAttributeCode())
        );
    }
}